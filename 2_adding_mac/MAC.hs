{-# LANGUAGE Trustworthy #-}

module MAC
    (
       H ()
     , L ()
     -- It comes from Core
     , Labeled ()
     , label
     , unlabel
     -- Monad MAC
     , MAC ()
     , runMAC
     -- Auxiliary proxies
     , fix
    )

where

import TCB

-- | To help the type-system
fix :: l -> MAC l ()
fix _l = return ()
