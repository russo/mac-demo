{-# LANGUAGE Trustworthy #-}
module Alice where

{-- Security library modules --}
import Lattice
import TCB

import qualified Bob

manager :: IO String
manager = do  putStr "Please, select your password:"
              pwd <- getLine
              -- bool <- Bob.common_pwd pass
              macH <- runMAC (untrusted pwd)
              bool <- runMAC macH
              if bool
                 then do putStrLn "Your password is too common!"
                         manager
                 else return pwd
    where
      untrusted :: String -> MAC L (MAC H Bool)
      untrusted pwd = do lpwd <- label pwd
                         Bob.common_pwd lpwd
