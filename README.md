# MAC's library demo #

Here, you can find the accompanying material for the PhD-level course 
[Information-Flow Control Libraries](http://www.cse.chalmers.se/~russo/fosad2017/)

## What do you need to run the examples?  

* [GHC](https://www.haskell.org/ghc/) 
* [NodeJS](https://nodejs.org/en/) 
* Make the domain name `bob.evil` to resolve to `localhost` 

Once you have all of that, you should start by setting up the attacker's server by running `node https://nodejs.org/en/`. 
This will run a web server that accepts requests and just displays them in the terminal. Once you have the attacker's 
web server running,  you simply need to load Alice's code by writing `ghci Alice.hs` in the right directory. 
After that, you just type `manager` in the terminal. 

## Structure of the repository  

The repository consists on many directories reflecting different aspects and insights involved in the development of 
security libraries in Haskell. Below, you can find a description for each directory as well as its structure. 

* `0_haskell`
	* `Intro.hs`: examples of side-effect free Haskell functions
	* `IntroEffects.hs`: examples of side-effectful code (monad `IO`)
* `1_plain`
	* `Alice.hs`: Alice's code for the password manager
	* `Bob.hs`: Bob's code for the password manager
	* `Wget.hs`: module which provides a wget-like functionality in Haskell

For the rest of the directories, the structure of files is as follows. 


* `Alice.hs` and `Bob.hs` are the modules for Alice and Bob's code, respectively.
* `Wget.hs` is the file providing wget-like functionality 
* `MACWget.hs` introduces wget-like functionality into the monad MAC as a public computaiton
* `Lattice.hs` defines the two-point lattice (low and high)
* `TCB.hs` is the file implementing the security library and exporting all its internals.
* `MAC.hs` is the file importing `TCB.hs` but only exporting a safe interface.

The difference among the following directories relies mainly on the content of `TCB.hs` or `Bob.hs` 

* `2_adding_mac`: it shows how Alice can use MAC to secure calling to Bob's code. 
* `3_adding_join`: it introduces the `join` primitive into the library (see `TCB.hs`)
* `4_attack_exception`: it shows how exceptions, which are naively added to `TCB.hs`, can be misused to leak information (see `Bob.hs`).
* `5_termination`: it shows how termination can be exploited to leak secrets (see `Bob.hs`)