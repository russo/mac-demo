{-# LANGUAGE Trustworthy #-}
{-# LANGUAGE TypeApplications #-}
module MACWget where

{-- Security library modules --}
import Lattice
import TCB

import Network.HTTP.Wget

wgetMAC :: String -> MAC L String
wgetMAC s = do 
    lstr <- ioTCB (wget s [] [])
    return $ concat @[] lstr 
