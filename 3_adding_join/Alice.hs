{-# LANGUAGE Trustworthy #-}
module Alice where

{-- Security library modules --}
import Lattice
import TCB

import qualified Bob

manager :: IO String
manager = do  putStr "Please, select your password:"
              pwd <- getLine
              -- macH <- runMAC (untrusted pwd)
              -- bool <- runMAC macH
              (LabeledTCB bool) <- runMAC (untrusted pwd)
              if bool
                 then do putStrLn "Your password is too common!"
                         manager
                 else return pwd
    where
      untrusted :: String -> MAC L (Labeled H Bool)
      untrusted pwd = do lpwd <- label pwd
                         Bob.common_pwd lpwd
