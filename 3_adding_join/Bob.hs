{-# LANGUAGE Safe #-}
module Bob where

import Data.Maybe
import Data.List
import Data.List.Split


{-- Security library --}
import MAC
import MACWget


common_pwd :: Labeled H String -> MAC L (Labeled H Bool)
common_pwd lpwd = do
  str <- wgetMAC "http://www.openwall.com/passwords/wordlists/password-2011.lst"
  let lines_ = filter (not.null) (linesBy (=='\n') str)
  let words_ = filter ( not . (=='#') . head ) lines_
  joinMAC (
           do pwd <- unlabel lpwd
              return ( isJust ( find (== pwd) words_ ) )
          )
