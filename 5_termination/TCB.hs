{-# LANGUAGE Unsafe #-}

module TCB
    (
       L ()
     , H ()
     -- Labeled expressions
     , Labeled (LabeledTCB)
     , label
     , unlabel
     -- Monad MAC
     , MAC (MkMAC)
     , runMAC
     , joinMAC
     -- Exceptions
     , throwMAC
     , catchMAC
     -- TCB operations
     , ioTCB
    )

where

import Control.Applicative
import Control.Exception

{-- Security levels --}
import Lattice


{-- Labeled values --}
newtype Labeled l a = LabeledTCB a

label      :: Less l l' => a -> MAC l (Labeled l' a)
unlabel    :: Less l' l => Labeled l' a -> MAC l a

{-- MAC's operations --}
newtype MAC l a = MkMAC (IO a)
runMAC  :: MAC l a -> IO a
joinMAC :: (Less l l') => MAC l' a -> MAC l (Labeled l' a)

{-- TCB operations --}
ioTCB      :: IO a -> MAC l a

{-- Exceptions --}
throwMAC :: Exception e => e -> MAC l a
catchMAC :: Exception e => MAC l a -> (e -> MAC l a) -> MAC l a



{---------------------}
{---------------------}
{-- Implementations --}
{---------------------}
{---------------------}

label v = return (LabeledTCB v)

unlabel (LabeledTCB v) = return v


instance Functor (MAC l) where
    fmap f (MkMAC io) = MkMAC (fmap f io)

instance Applicative (MAC l) where
    pure = MkMAC . return
    (<*>) (MkMAC f) (MkMAC a) = MkMAC (f <*> a)

instance Monad (MAC l) where
   return = pure
   MkMAC m >>= k = ioTCB (m >>= runMAC . k)

ioTCB = MkMAC

runMAC (MkMAC m) = m

joinMAC m = ioTCB (runMAC (catch_any_exception m))
            where
                  catch_any_exception m' =
                       catchMAC
                                (do v <- m'
                                    return (LabeledTCB v))
                                hd

                  hd :: SomeException -> MAC l (Labeled l' a)
                  hd e = return (LabeledTCB (throw e))



throwMAC = ioTCB . throw
catchMAC (MkMAC io) hd = ioTCB ( catch io (runMAC . hd) )
